# 旅见旅闻 iOS App 与页面交互规则
## UserAgent
    TravelApp-iOS
## 接口定义
#### App提供给JS的接口
```JS
//获取当前登录用户信息
window.WebViewJavascriptBridge.callHandler('getUserInfo', 
        '',		// JS 调用该原生handler传给App的参数，该handler不须传参，这里传''或者undefined均可
        function(response){
        		// 在这里解析App返回结果
        });

//获取当前经纬度
window.WebViewJavascriptBridge.callHandler('getLocation', 
        '',
        function(response){
        });

//触发App Toast
window.WebViewJavascriptBridge.callHandler('showToast', 
        'json parameters');

//在新的WebView中打开指定地址
window.WebViewJavascriptBridge.callHandler('openInNewWebPage', 
        'json parameters');
        
//设定地图缩放等级
window.WebViewJavascriptBridge.callHandler('setMapZoomLevel', 
        'json parameters');
        
//拉起百度地图导航
window.WebViewJavascriptBridge.callHandler('openInNavigation', 
        'json parameters');

//回到首页
window.WebViewJavascriptBridge.callHandler('backHome');

//拉起分享工菜单
window.WebViewJavascriptBridge.callHandler('openShareView', 
        'json parameters');

//播放音频 By 景区ID
window.WebViewJavascriptBridge.callHandler('playRegionAudio', 
        'json parameters');
        
//暂停播放
window.WebViewJavascriptBridge.callHandler('stopPlayingAudio');

//继续播放
window.WebViewJavascriptBridge.callHandler('resumePlayingAudio');

//拉起登录页面
window.WebViewJavascriptBridge.callHandler('openLoginView');

// startDownload
window.WebViewJavascriptBridge.callHandler('startDownload', 
        'json parameters');
        
//获取音频播放当前状态
window.WebViewJavascriptBridge.callHandler('getPlayerCurrentState', 
        '',		// JS 调用该原生handler传给App的参数，该handler不须传参，这里传''或者undefined均可
        function(response){
        		// 在这里解析App返回结果
        });

//拉起搜索界面
window.WebViewJavascriptBridge.callHandler('openSearch');

//获取当前语言
window.WebViewJavascriptBridge.callHandler('getCurrentLanguage', 
        '',		// JS 调用该原生handler传给App的参数，该handler不须传参，这里传''或者undefined均可
        function(response){
        		// 在这里解析App返回结果
        });
```
#### JS给App提供的接口
暂无
后续需要JS提供接口给App调用可以按以下方式提供
```JS
window.WebViewJavascriptBridge.registerHandler('someJavascriptHandler', function(data, responseCallback) {
	log('ObjC called testJavascriptHandler with', data);
	var responseData = { 'Javascript Says':'Right back atcha!' };
	log('JS responding with', responseData);
	responseCallback(responseData);
});
```
#### 示例代码
```JS
function connectWebViewJavascriptBridge(callback) {
    if (window.WebViewJavascriptBridge) {
        callback(WebViewJavascriptBridge);
    } else {
        document.addEventListener('WebViewJavascriptBridgeReady', function() {
            callback(WebViewJavascriptBridge);
        }, false);
    }
}

connectWebViewJavascriptBridge((function(bridge) {
    bridge.init();
})
```
## 参数具体格式
#### getUserinInfo
```JS
response = {
    isLogin:int 	//0-未登录, 1-已登录
    user:{
		userId:string 		//用户id
		userName:string	//用户昵称
		hreadUrl:string	//头像地址
		token:string		//token
	}
};
```
#### getLocation
```JS
response = {
	status:int	//0-成功, 1-失败（未取得用户授权）
	location = {
	  	lng:double //经度
	  	lat:double //纬度
  	}
};
```
#### showToast
```JS
'json parameters' = {
    msg:string //Toast 内容
};
```
#### openInNewWebPage
```JS
'json parameters' = {
    URL:string //要打开的网址，不用 URLEncode
};
```
#### setMapZoomLevel
```JS
'json parameters' = {
    level:int //缩放等级，3-21，默认14，数值越大地图越放大
};
```
#### openInNavigation
```JS
'json parameters' = {
    name:string, 	//目标地名称
    lat:double,		//目标地纬度
    lng:double		//目标地经度
};
```
#### openShareView
```JS
'json parameters' = {
    title:string, 		//分享标题
    desc:string,		//分享内容
    imageURL:string,	//图片URL，不需要URLEncode
    shareURL:string		//分享链接地址，不需要URLEncode
};
```
#### playRegionAudio
```JS
'json parameters' = {
    regionVoice:string 	//景区mp3地址
};
```
#### startDownload
```JS
'json parameters' = {
    name:string, 		//名称
    pic:string,			//图片地址
    zipURL:string,		//压缩包地址
    desc:string			//描述
    otherDesc:string	//其他描述
};
```
#### getPlayerCurrentState
```JS
response = {
	isPlaying:int			//1-正在播放, 0-不在播放
	playingAdress:string 	//正在播放的音频地址，不在播放时值为 ''
};
```
#### getCurrentLanguage
```JS
response = {
    language:string 	//zh-汉语, en-英语
};